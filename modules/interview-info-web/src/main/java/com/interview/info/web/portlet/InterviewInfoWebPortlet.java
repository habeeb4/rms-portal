package com.interview.info.web.portlet;

import java.text.ParseException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.interview.info.model.InterviewInfo;
import com.interview.info.service.InterviewInfoLocalServiceUtil;
import com.interview.info.web.constants.InterviewInfoWebPortletKeys;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.prime.util.helper.portlet.DateHelperUtil;
import com.prime.util.helper.portlet.FileHelper;

/**
 * @author E2Software-011
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=InterviewInfoWeb",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + InterviewInfoWebPortletKeys.INTERVIEWINFOWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class InterviewInfoWebPortlet extends MVCPortlet {
	private static final Log logger = LogFactoryUtil.getLog(InterviewInfoWebPortlet.class);
	public void addInterview(ActionRequest actionRequest, ActionResponse actionResponse) throws ParseException, PortalException {
//		ThemeDisplay themeDisplay= (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String interviewName = ParamUtil.getString(actionRequest, "interviewName");
		long clientName = ParamUtil.getLong(actionRequest, "clientName");
		String fromTime = ParamUtil.getString(actionRequest, "fromTime");
		String[] interviewers = ParamUtil.getStringValues(actionRequest, "interviewers");
		String location = ParamUtil.getString(actionRequest, "location");
		long candidateName = ParamUtil.getLong(actionRequest, "candidateName");
		long postingTitle = ParamUtil.getLong(actionRequest, "postingTitle");
		String toTime = ParamUtil.getString(actionRequest, "toTime");
		long interviewOwner = ParamUtil.getLong(actionRequest, "interviewOwner");
		String scheduleComments = ParamUtil.getString(actionRequest, "scheduleComments");
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		List<FileEntry> fileEntryIds = FileHelper.fileUpload(uploadPortletRequest, "Interview Information");
		long convertedFrom = DateHelperUtil.convertToMillis(fromTime);
		long convertedTo = DateHelperUtil.convertToMillis(toTime);
		logger.info("Interview Name:::::"+interviewName);
		logger.info("Client name:::::"+clientName);
		logger.info("From time::::::::::::::::"+fromTime);
		logger.info("Interviewers:::::"+interviewers);
		logger.info("Location:::::"+location);
		logger.info("candidate Name:::::::::"+candidateName);
		logger.info("Posting title::::::::"+postingTitle);
		logger.info("To time::::::" +toTime);
		logger.info("Interview owner:::::" +interviewOwner);
		logger.info("Schedule comments::::::"+scheduleComments);
		InterviewInfo interviewInfo =  InterviewInfoLocalServiceUtil.createInterviewInfo(WorkflowConstants.STATUS_APPROVED);
		interviewInfo.setInterviewName(interviewName);
		interviewInfo.setClientId(clientName);
		interviewInfo.setInterviewFrom(convertedFrom);
		interviewInfo.setInterviewers(String.join(StringPool.COMMA, interviewers));
		interviewInfo.setLocation(location);
		interviewInfo.setCandidateId(candidateName);
		interviewInfo.setPostingTitleId(postingTitle);
		interviewInfo.setInterviewTo(convertedTo);
		interviewInfo.setInterviewOwner(interviewOwner);
		interviewInfo.setScheduleComments(scheduleComments);
		for(FileEntry fe: fileEntryIds) {
			interviewInfo.setAttachmentId(fe.getFileEntryId());
		}
		InterviewInfoLocalServiceUtil.addInterviewInfo(interviewInfo);
	}
}