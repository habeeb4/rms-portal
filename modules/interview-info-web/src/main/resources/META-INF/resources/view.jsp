<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletMode"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
<%@page import="javax.portlet.RenderResponse"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@page import="com.liferay.document.library.kernel.util.DLUtil"%>
<%@page import="com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.liferay.petra.string.StringPool"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="java.util.Date"%>
<%@page import="com.prime.util.helper.portlet.DateHelperUtil"%>
<%@page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.interview.info.service.InterviewInfoLocalServiceUtil"%>
<%@page import="com.interview.info.model.InterviewInfo"%>
<%@ page import="com.liferay.document.library.kernel.model.DLFileEntry" %>
<%@ page import="com.liferay.portal.kernel.repository.model.FileEntry" %>
<%@ include file="/init.jsp" %>
<portlet:renderURL var="renderURL">
	<portlet:param name="mvcPath" value="/create-interview.jsp" />
</portlet:renderURL>
<aui:button type="submit" onClick="${renderURL}" value="Create Interview"></aui:button>
<%
	List<InterviewInfo> interviewInfo = InterviewInfoLocalServiceUtil.getInterviewInfos(-1, -1);
%>
<aui:form method="post" name="fm">
<liferay-ui:search-container total="<%=interviewInfo.size()%>"
	var="searchContainer" delta="10" deltaConfigurable="true"
	emptyResultsMessage="No Interviews To Display" rowChecker="<%=new RowChecker(renderResponse)%>">
	<liferay-ui:search-container-results
		results="<%=ListUtil.subList(interviewInfo, searchContainer.getStart(), searchContainer.getEnd())%>" />
	<liferay-ui:search-container-row className="com.interview.info.model.InterviewInfo" modelVar="ii" keyProperty="interviewId">
		<%
		String interviewFromCon = DateHelperUtil.convertToDate(ii.getInterviewFrom());
		String interviewToCon = DateHelperUtil.convertToDate(ii.getInterviewTo());
		String createdDateCon = DateHelperUtil.convertToDate(ii.getCreatedDate());
		String modifiedByCon = DateHelperUtil.convertToDate(ii.getModifiedBy());
		%>
		<liferay-ui:search-container-column-text name="Interview ID" property="interviewId" />
		<liferay-ui:search-container-column-text name="Interview Name" property="interviewName" />
		<liferay-ui:search-container-column-text name="Posting Title" property="postingTitleId" />
		<liferay-ui:search-container-column-text name="Candidate Name" property="candidateId" />
		<liferay-ui:search-container-column-text name="Client Name" property="clientId" />
		<liferay-ui:search-container-column-text name="Interview From"><%=interviewFromCon%></liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="Interview To"><%=interviewToCon%></liferay-ui:search-container-column-text>
		<%
		if(Validator.isNotNull(ii.getAttachmentId())){
			DLFileEntry dlFileEntry = DLFileEntryLocalServiceUtil.getDLFileEntry(ii.getAttachmentId());
		%>
		<liferay-ui:search-container-column-text name="Attached File"><a href='<%=themeDisplay.getPortalURL()+"/c/document_library/get_file?uuid="+dlFileEntry.getUuid()+"&groupId="+themeDisplay.getScopeGroupId()%>' target="_blank"><%=dlFileEntry.getFileName()%>
		</a></liferay-ui:search-container-column-text>
		<%
			} else {
		%>
		<liferay-ui:search-container-column-text name="Attached File">No files found</liferay-ui:search-container-column-text>
		<%
			}
		%>
		<liferay-ui:search-container-column-text name="Interviewers" property="interviewers" />
		<liferay-ui:search-container-column-text name="Interview Owner" property="interviewOwner" />
		<liferay-ui:search-container-column-text name="Schedule comments" property="scheduleComments" />
		<liferay-ui:search-container-column-text name="Location" property="location" />
		<liferay-ui:search-container-column-text name="Created By" property="createdBy" />
		<liferay-ui:search-container-column-text name="Created Date"><%=createdDateCon%></liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="Modified By" property="modifiedBy" />
		<liferay-ui:search-container-column-text name="Modified Date"><%=modifiedByCon%></liferay-ui:search-container-column-text>
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator />
</liferay-ui:search-container>
<aui:button-row>
    <aui:button value="delete" onClick="showSelectedFoos();" />
</aui:button-row>
</aui:form>
<script>
Liferay.provide(
        window,
        '<portlet:namespace />showSelectedFoos',
         function() {
                    var cmd = 
                           Liferay.Util.listCheckedExcept(
                               document.<portlet:namespace />show_fm,   "<portlet:namespace />allRowIds"); 
                    if(cmd==""||cmd==null){
                         alert("Please select atleast one foo to delete");
                        return false;
                    }

                    if (confirm("Are you sure you want to delete Foos? ")) {

                        var actionURL = Liferay.PortletURL.createActionURL();
                        actionURL.setWindowState("<%=LiferayWindowState.NORMAL.toString() %>");
                        actionURL.setPortletMode("<%=LiferayPortletMode.VIEW %>");
                        actionURL.setParameter("selectedRows",cmd);
                        actionURL.setParameter("javax.portlet.action","deleteMultiple");
                        actionURL.setPortletId("<%=themeDisplay.getPortletDisplay().getId() %>");

                        submitForm(document.<portlet:namespace />show_fm, actionURL);

                }
          },
          ['liferay-util-list-fields', 'liferay-portlet-url']
);

</script>
