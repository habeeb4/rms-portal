package com.prime.util.helper.portlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.model.DLVersionNumberIncrease;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.FileItem;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

public class FileHelper {
	private FileHelper() {
	}
	private static final Log logger = LogFactoryUtil.getLog(FileHelper.class);
	private static String ROOT_FOLDER_DESCRIPTION = "This folder is for Interviews";
	private static long PARENT_FOLDER_ID = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
	public static List<FileEntry> fileUpload(UploadPortletRequest uploadPortletRequest,String folderName) throws PortalException {
			List<FileEntry> fileEntries = new ArrayList<>();
			long fileEntryId = 0;
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(), uploadPortletRequest);
			ThemeDisplay themeDisplay = (ThemeDisplay) uploadPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long repositoryId = themeDisplay.getScopeGroupId();
			Folder folder = getFolder(uploadPortletRequest, folderName);
			if (Validator.isNull(folder)) {
				folder = createNewFolder(uploadPortletRequest, folderName);
			}
			Map<String, FileItem[]> files= uploadPortletRequest.getMultipartParameterMap();
			logger.info("files..."+files.size());
			for (Entry<String, FileItem[]> file2 : files.entrySet()) {
				FileItem item[] = file2.getValue();
				try {
					for (FileItem fileItem : item) {
						FileEntry fileEntry = null;
						InputStream is;
						File file;
						String title,description,mimeType;
						mimeType = fileItem.getContentType();	
						file = fileItem.getStoreLocation();
						is = fileItem.getInputStream();
						title=fileItem.getFileName();
						description = title;
						FileEntry existingfileEntry = null;
						try {
						 existingfileEntry = DLAppLocalServiceUtil.getFileEntry(repositoryId,folder.getFolderId(), title);
							logger.info("existingfileEntry : "+existingfileEntry);
						}catch (PortalException e) {
							logger.error(e.getMessage(),e);
						}
					try {
							if(Validator.isNotNull(existingfileEntry)) {
							 fileEntry = DLAppServiceUtil.updateFileEntry(existingfileEntry.getFileEntryId(), existingfileEntry.getFileName(), mimeType, title, title, description, "", DLVersionNumberIncrease.MAJOR, is, existingfileEntry.getSize(),null,null, serviceContext);
							}else {
							fileEntry = DLAppServiceUtil.addFileEntry(repositoryId, folder.getFolderId(), title,mimeType, title, description, "", file, serviceContext);
							fileEntryId = fileEntry.getFileEntryId();
							logger.info("Multiple file entries are - - - > " +fileEntryId);
						}
							fileEntries.add(fileEntry);
					} catch (PortalException e) {
						logger.error(e.getMessage(),e);
					}		 
				}
			} catch (IOException e) {
				logger.error(e.getMessage(),e);
			}
		}
			return fileEntries;
	}
	
		public static Folder createNewFolder(UploadPortletRequest uploadPortletRequest, String folderName){
			ThemeDisplay themeDisplay = (ThemeDisplay) uploadPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Folder folder = null;
				long repositoryId = themeDisplay.getScopeGroupId();
				try {
					ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFolder.class.getName(),
							uploadPortletRequest);
					logger.info("Creating folder name with this name- - - >" +folderName);
					folder = DLAppLocalServiceUtil.addFolder(themeDisplay.getUserId(), repositoryId, PARENT_FOLDER_ID,
							folderName, ROOT_FOLDER_DESCRIPTION, serviceContext);
				} catch (Exception e) {
					logger.error(e.getMessage(),e);
				}
			return folder;
		}

		public static Folder getFolder(UploadPortletRequest uploadPortletRequest, String folderName) {
			ThemeDisplay themeDisplay = (ThemeDisplay) uploadPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Folder folder = null;
			try {
				folder = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), PARENT_FOLDER_ID, folderName);
				logger.info(folderName + " Exists::::::");
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			return folder;
		}

		public static String getFileUrlByFileEntry(long fileEntryId,ThemeDisplay themeDisplay) throws PortalException {
			FileEntry fileEntry	= DLAppServiceUtil.getFileEntry(fileEntryId);
			String url = themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId() + "/" + fileEntry.getFolderId() + "/" + fileEntry.getTitle();
			return url;
			}
}
