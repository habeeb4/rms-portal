package com.prime.util.helper.portlet;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.interview.info.model.InterviewInfo;
import com.interview.info.service.InterviewInfoLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

public class Classes  implements MVCRenderCommand {
	public static final void get(){
		String myObj = "";
	if(Validator.isNotNull(myObj)) {
		System.out.println(myObj+"Object printed::::::::");
	}
	Optional<Object> emptyObj = Optional.empty();
	System.out.println(emptyObj+"Empty obj printed:::::");
	}

	@Override
	public String render(RenderRequest arg0, RenderResponse arg1) throws PortletException {
		long interviewId = ParamUtil.getLong(arg0, "interviewId");
		InterviewInfo ii = null;
		get();
		try {
			ii = InterviewInfoLocalServiceUtil.getInterviewInfo(interviewId);
		} catch (PortalException e) {
			e.printStackTrace();
		}
		System.out.println(ii);
		return ii.toString();
	}	
}