package com.prime.util.helper.constants;

/**
 * @author Vinoth-kumar
 */
public class PrimeHelperUtilPortletKeys {

	public static final String PRIMEHELPERUTIL =
		"com_prime_util_helper_PrimeHelperUtilPortlet";
	
	public static final String HELPER_MESSAGE = "This helper util helps reduce time";
}