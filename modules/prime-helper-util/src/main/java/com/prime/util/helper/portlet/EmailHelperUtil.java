package com.prime.util.helper.portlet;

import java.io.IOException;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class EmailHelperUtil {
	private static final Log _log = LogFactoryUtil.getLog(EmailHelperUtil.class);
	private EmailHelperUtil() {
	}
	public static void sendMail(String to, String subject, String body) throws IOException {
		MailMessage mailMessage = new MailMessage();
		try {
		InternetAddress fromAddress = new InternetAddress("xyz@yopmail.com"); 
		InternetAddress toAddress = new InternetAddress (to);
		mailMessage.setFrom(fromAddress); 
		mailMessage.setTo(toAddress);
		mailMessage.setSubject(subject);
		mailMessage.setBody(body);
		MailServiceUtil.sendEmail(mailMessage);
		_log.info("Mail sent:::");
		} catch (AddressException e) {
		_log.info(e.getMessage());
		}
		}
	}
