package com.prime.util.helper.portlet;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.portlet.ActionRequest;

import com.liferay.portal.kernel.util.ParamUtil;
import com.prime.util.helper.constants.PrimeHelperUtilPortletKeys;

public class DateHelperUtil {
	
	public String helperUtil() {
		return PrimeHelperUtilPortletKeys.HELPER_MESSAGE;
	}
	
	public static Date convertToDate(ActionRequest actionRequest, String date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
		return ParamUtil.getDate(actionRequest, date, simpleDateFormat);		
	}
	
	public static long getCurrentMillis() {
		return System.currentTimeMillis();
	}
	public static long convertToMillis(String myDate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		Date date = sdf.parse(myDate);
		return date.getTime();
	}
	public static String convertToDate(long millis) {
		DateFormat obj = new SimpleDateFormat("dd MMM yyyy HH:mm");   
        Date res = new Date(millis);
		return obj.format(res);
	}
}